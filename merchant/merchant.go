package merchant

import (
	hashids "github.com/speps/go-hashids"
	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type Merchant struct {
	ID          bson.ObjectId `bson:"_id", json:"_id"`
	Name        string        `bson:"name", json:"name"`
	BankAccount string        `bson:"bank_account", json:"bank_account"`
	Username    string        `bson:"username", json:"username"`
	Password    string        `bson:"password", json:"password"`
}

type Service struct {
	DB *mgo.Database
}

const (
	collections       = "merchants"
	reportcollections = "reports"
)

func (s *Service) CreateMerchant(m *Merchant) error {
	m.ID = bson.NewObjectId()
	// encodedPass, _ := encodePin(m.Password, m.Username)
	// m.Password = encodedPass
	if err := s.DB.C(collections).Insert(&m); err != nil {
		return err
	}
	return nil
}

func (s *Service) GetMerchantByBankAccount(bankAccount string) (*Merchant, error) {
	merchant := &Merchant{}
	if err := s.DB.C(collections).Find(bson.M{"bank_account": bankAccount}).One(&merchant); err != nil {
		return nil, err
	}
	// decodedPass, _ := decodePin(merchant.Password, merchant.Username)
	// merchant.Password = decodedPass
	return merchant, nil
}

func (s *Service) CheckBankAccountExists(bankAccount string) (bool, error) {
	err := s.DB.C(collections).Find(bson.M{"bank_account": bankAccount}).One(nil)
	if err != nil {
		return false, err
	}
	return true, nil
}

func (s *Service) GetMerchantByID(id bson.ObjectId) (*Merchant, error) {
	merchant := &Merchant{}
	if err := s.DB.C(collections).Find(bson.M{"_id": id}).One(&merchant); err != nil {
		return nil, err
	}
	// decodedPass, _ := decodePin(merchant.Password, merchant.Username)
	// merchant.Password = decodedPass
	return merchant, nil
}
func (s *Service) All() ([]Merchant, error) {
	var merchants []Merchant
	err := s.DB.C(collections).Find(nil).All(&merchants)
	if err != nil {
		return nil, err
	}
	return merchants, nil
}

func (s *Service) Update(id bson.ObjectId, name string) error {
	err := s.DB.C(collections).Update(bson.M{"_id": id}, bson.M{"$set": bson.M{"name": name}})
	if err != nil {
		return err
	}
	return nil
}

func (s *Service) Delete(id bson.ObjectId) error {
	return nil
}

type SellReportResponse struct {
	Date       string        `bson:"date", json:"date"`
	Products   []SoldProduct `bson:"products", json:"products"`
	Accumulate float64       `bson:"accumulate", json:"accumulate"`
}

type SoldProduct struct {
	Name          string `bson:"name", json:"name"`
	SellingVolume int    `bson:"selling_volume", json:"selling_volume"`
}

func (s *Service) GetSellReports(id bson.ObjectId) ([]SellReportResponse, error) {
	var sellReport []SellReportResponse
	err := s.DB.C(reportcollections).Find(bson.M{"merchant_id": id}).All(&sellReport)
	if err != nil {
		return nil, err
	}
	return sellReport, nil
}

func (s *Service) ValidateUsernamePassword(username string, password string) error {
	err := s.DB.C(collections).Find(bson.M{"username": username, "password": password}).One(nil)
	if err != nil {
		return err
	}
	return nil
}

func encodePin(pin string, username string) (string, error) {
	hd := hashids.NewData()
	hd.Salt = username
	hd.MinLength = 30
	h, err := hashids.NewWithData(hd)

	if err != nil {
		return "", err
	}
	e, err := h.EncodeHex(pin)

	if err != nil {
		return "", err
	}

	return e, nil
}

func decodePin(encodedPin, username string) (string, error) {
	hd := hashids.NewData()
	hd.Salt = username
	hd.MinLength = 30
	h, err := hashids.NewWithData(hd)
	if err != nil {
		return "", err
	}
	d, err := h.DecodeHex(encodedPin)
	if err != nil {
		return "", err
	}
	return d, nil
}
