package merchant

import (
	"errors"
	"fmt"
	"kpay-merchant/response"
	"log"
	"net/http"

	"github.com/bxcodec/faker"
	"github.com/gin-gonic/gin"
	"gopkg.in/mgo.v2/bson"
)

type MerchantService interface {
	GetMerchantByID(id bson.ObjectId) (*Merchant, error)
	All() ([]Merchant, error)
	CreateMerchant(m *Merchant) error
	GetMerchantByBankAccount(bankAccount string) (*Merchant, error)
	CheckBankAccountExists(bankAccount string) (bool, error)
	Update(id bson.ObjectId, name string) error
	Delete(id bson.ObjectId) error
	GetSellReports(id bson.ObjectId) ([]SellReportResponse, error)
	ValidateUsernamePassword(username string, password string) error
}

type MerchantHandler struct {
	MerchantService MerchantService
}

func (h *MerchantHandler) RegisterMerchant(c *gin.Context) {
	var registerRequest struct {
		Name        string `json:"name" binding:"required"`
		BankAccount string `json:"bank_account" binding:"required"`
	}
	err := c.Bind(&registerRequest)
	if err != nil {
		response.PrintBadRequest(c, err.Error())
		log.Printf("Get JSON failed")
		return
	}

	bankAccExist, err := h.MerchantService.CheckBankAccountExists(registerRequest.BankAccount)
	if bankAccExist {
		response.PrintBadRequest(c, errors.New("Bank account already exists in the system").Error())
		return
	} else if err != nil && err.Error() != "not found" {
		response.PrintBadRequest(c, err.Error())
		return
	}

	user, pass := generateUsernamePassword()
	newMerchant := &Merchant{
		Name:        registerRequest.Name,
		BankAccount: registerRequest.BankAccount,
		Username:    user,
		Password:    pass,
	}

	err = h.MerchantService.CreateMerchant(newMerchant)
	if err != nil {
		response.PrintBadRequest(c, err.Error())
		return
	}
	response.PrintSuccess(c)
	c.JSON(http.StatusOK, gin.H{
		"merchant_info": newMerchant,
	})
}

func (h *MerchantHandler) GetMerchantInfo(c *gin.Context) {
	id := c.Param("id")
	objID := bson.ObjectIdHex(id)
	merchant, err := h.MerchantService.GetMerchantByID(objID)
	if err != nil {
		response.PrintBadRequest(c, err.Error())
		return
	}
	c.JSON(http.StatusOK, gin.H{
		"merchant_info": merchant,
	})
}

func (h *MerchantHandler) UpdateMerchant(c *gin.Context) {
	id := c.Param("id")
	objID := bson.ObjectIdHex(id)

	var updateRequest struct {
		Name string `json:"name" binding:"required"`
	}
	err := c.Bind(&updateRequest)
	if err != nil {
		response.PrintBadRequest(c, err.Error())
		log.Printf("Get JSON failed")
		return
	}

	err = h.MerchantService.Update(objID, updateRequest.Name)
	if err != nil {
		response.PrintBadRequest(c, err.Error())
		return
	}

	response.PrintSuccess(c)
}

func (h *MerchantHandler) GetSellReports(c *gin.Context) {
	id := c.Param("id")
	objID := bson.ObjectIdHex(id)
	sellReport, err := h.MerchantService.GetSellReports(objID)
	if err != nil {
		response.PrintBadRequest(c, err.Error())
		return
	}
	c.JSON(http.StatusOK, gin.H{
		"merchant_info": sellReport,
	})
}

func generateUsernamePassword() (string, string) {
	var userAccount struct {
		Password string `faker:"password"`
		UserName string `faker:"username"`
	}
	err := faker.FakeData(&userAccount)
	if err != nil {
		fmt.Println(err)
	}
	return userAccount.UserName, userAccount.Password
}

func (h *MerchantHandler) MerchantAuthentication(c *gin.Context) {
	id := c.Param("id")
	objID := bson.ObjectIdHex(id)
	username, password, ok := c.Request.BasicAuth()
	if ok {
		if username == "" || password == "" {
			c.AbortWithStatus(http.StatusUnauthorized)
		}

		merchant, err := h.MerchantService.GetMerchantByID(objID)
		if err != nil {
			response.PrintBadRequest(c, err.Error())
			return
		}
		if merchant.Name == username || merchant.Password == password {
			return
		}
	}
	c.AbortWithStatus(http.StatusUnauthorized)
}
