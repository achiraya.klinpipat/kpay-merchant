package response

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

func PrintBadRequest(c *gin.Context, message string) {
	c.JSON(http.StatusBadRequest, gin.H{
		"message": message,
	})
}

func PrintOK(c *gin.Context, message string) {
	c.JSON(http.StatusOK, gin.H{
		"message": message,
	})
}

func PrintSuccess(c *gin.Context) {
	PrintOK(c, "Success")
}
