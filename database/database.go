package database

import (
	"log"

	"github.com/BurntSushi/toml"
	"gopkg.in/mgo.v2"
)

type Database struct {
	DBSession *mgo.Session
	DB        *mgo.Database
	Server    string
	DBName    string
}

type DBConfig struct {
	Server   string
	Database string
}

var DB *Database

func (db *Database) Connect() *mgo.Database {
	var dbConfig DBConfig
	_, err := toml.DecodeFile("database/dbconfig.toml", &dbConfig)
	if err != nil {
		log.Fatal(err)
	}

	session, err := mgo.Dial(dbConfig.Server)
	if err != nil {
		log.Fatal(err)
	}
	db = &Database{session, session.DB(dbConfig.Database), dbConfig.Server, dbConfig.Database}
	return db.DB
}
