package products

import (
	"errors"
	"fmt"
	"kpay-merchant/response"
	"log"
	"net/http"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
	"gopkg.in/mgo.v2/bson"
)

type ProductsService interface {
	FindByID(id bson.ObjectId) (*Product, error)
	FindProduct(mId bson.ObjectId, productName string) (*Product, error)
	GetAllProducts(mId bson.ObjectId) ([]Product, error)
	AddProduct(mId bson.ObjectId, p *Product) error
	UpdateProduct(mId bson.ObjectId, pID bson.ObjectId, amount float64) error
	DeleteProduct(mId bson.ObjectId, pID bson.ObjectId) error
	BuyProduct(sellReport *SellReport, pID bson.ObjectId, productName string, volume int) error
}

type ProductsHandler struct {
	ProductsService ProductsService
}

func (h *ProductsHandler) AllProducts(c *gin.Context) {
	id := c.Param("id")
	objID := bson.ObjectIdHex(id)

	products, err := h.ProductsService.GetAllProducts(objID)
	if err != nil {
		response.PrintBadRequest(c, err.Error())
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"products": products,
	})
}

func (h *ProductsHandler) AddProduct(c *gin.Context) {
	id := c.Param("id")
	objID := bson.ObjectIdHex(id)

	var addProductRequest struct {
		Name   string  `json:"name" binding:"required"`
		Amount float64 `json:"amount" binding:"required"`
	}
	err := c.Bind(&addProductRequest)
	if err != nil {
		response.PrintBadRequest(c, err.Error())
		log.Printf("Get JSON failed")
		return
	}

	mProducts, err := h.ProductsService.GetAllProducts(objID)
	if len(mProducts) >= 5 {
		response.PrintBadRequest(c, errors.New("You reached the product limit (5 products allowed)").Error())
		return
	} else if err != nil {
		response.PrintBadRequest(c, err.Error())
		return
	}

	amountStr := fmt.Sprintf("%.2f", addProductRequest.Amount)
	amount, _ := strconv.ParseFloat(amountStr, 64)

	newProduct := &Product{
		Name:   addProductRequest.Name,
		Amount: amount,
	}

	err = h.ProductsService.AddProduct(objID, newProduct)
	if err != nil {
		response.PrintBadRequest(c, err.Error())
		return
	}
	response.PrintSuccess(c)
}

func (h *ProductsHandler) UpdateProduct(c *gin.Context) {
	id := c.Param("id")
	pId := c.Param("product_id")
	objID := bson.ObjectIdHex(id)
	productID := bson.ObjectIdHex(pId)

	var updateProductRequest struct {
		Amount float64 `json:"amount" binding:"required"`
	}
	err := c.Bind(&updateProductRequest)
	if err != nil {
		response.PrintBadRequest(c, err.Error())
		log.Printf("Get JSON failed")
		return
	}

	amountStr := fmt.Sprintf("%.2f", updateProductRequest.Amount)
	amount, _ := strconv.ParseFloat(amountStr, 64)

	err = h.ProductsService.UpdateProduct(objID, productID, amount)
	if err != nil {
		response.PrintBadRequest(c, err.Error())
		return
	}
	response.PrintSuccess(c)
}

func (h *ProductsHandler) DeleteProduct(c *gin.Context) {
	id := c.Param("id")
	pId := c.Param("product_id")
	objID := bson.ObjectIdHex(id)
	productID := bson.ObjectIdHex(pId)

	err := h.ProductsService.DeleteProduct(objID, productID)
	if err != nil {
		response.PrintBadRequest(c, err.Error())
		return
	}
	response.PrintSuccess(c)
}

func (h *ProductsHandler) BuyProduct(c *gin.Context) {
	var buyProductRequest struct {
		MerchantID  string `json:"merchant_id" binding:"required"`
		ProductName string `json:"product_name" binding:"required"`
		Volume      int    `json:"volume" binding:"required"`
	}
	err := c.Bind(&buyProductRequest)
	if err != nil {
		response.PrintBadRequest(c, err.Error())
		log.Printf("Get JSON failed")
		return
	}
	merchantID := bson.ObjectIdHex(buyProductRequest.MerchantID)

	product, err := h.ProductsService.FindProduct(merchantID, buyProductRequest.ProductName)
	if err != nil {
		response.PrintBadRequest(c, err.Error())
		return
	}
	totalAmount := product.Amount * float64(buyProductRequest.Volume)

	date := time.Now()
	sellReport := &SellReport{
		MerchantID: merchantID,
		Date:       date.Format("2006-01-02"),
		Products:   []SoldProduct{},
		Accumulate: totalAmount,
	}

	productSold := SoldProduct{
		Name:          buyProductRequest.ProductName,
		SellingVolume: buyProductRequest.Volume,
	}

	sellReport.Products = append(sellReport.Products, productSold)

	err = h.ProductsService.BuyProduct(sellReport, product.ID, product.Name, buyProductRequest.Volume)
	if err != nil {
		response.PrintBadRequest(c, err.Error())
		return
	}

	response.PrintSuccess(c)
}
