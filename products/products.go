package products

import (
	"errors"

	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

const (
	collections       = "products"
	reportcollections = "reports"
)

type Product struct {
	ID            bson.ObjectId `bson:"_id", json:"_id"`
	MerchantID    bson.ObjectId `bson:"merchant_id", json:"merchant_id"`
	Name          string        `bson:"name", json:"name"`
	Amount        float64       `bson:"amount", json:"amount"`
	AmountChanges []float64     `bson:"amount_changes", json:"amount_changes"`
	SellingVolume int           `bson:"selling_volume", json:"selling_volume"`
}

type Service struct {
	DB *mgo.Database
}

func (s *Service) FindByID(id bson.ObjectId) (*Product, error) {
	var product *Product
	err := s.DB.C(collections).Find(bson.M{"_id": id}).One(&product)
	if err != nil {
		return nil, err
	}
	return product, nil
}

func (s *Service) FindProduct(mId bson.ObjectId, productName string) (*Product, error) {
	var product *Product
	err := s.DB.C(collections).Find(bson.M{"merchant_id": mId, "name": productName}).One(&product)
	if err != nil {
		return nil, err
	}
	return product, nil
}

func (s *Service) GetAllProducts(mId bson.ObjectId) ([]Product, error) {
	var products []Product
	err := s.DB.C(collections).Find(bson.M{"merchant_id": mId}).All(&products)
	if err != nil {
		return nil, err
	}
	return products, nil
}

func (s *Service) AddProduct(mId bson.ObjectId, p *Product) error {
	p.ID = bson.NewObjectId()
	p.MerchantID = mId
	p.SellingVolume = 0
	p.AmountChanges = []float64{p.Amount}
	if err := s.DB.C(collections).Insert(&p); err != nil {
		return err
	}
	return nil
}

func (s *Service) UpdateProduct(mId bson.ObjectId, pID bson.ObjectId, amount float64) error {
	err := s.DB.C(collections).Update(bson.M{"_id": pID, "merchant_id": mId}, bson.M{"$set": bson.M{"amount": amount}, "$push": bson.M{"amount_changes": amount}})
	if err != nil {
		return err
	}
	return nil
}

func (s *Service) DeleteProduct(mId bson.ObjectId, pID bson.ObjectId) error {
	err := s.DB.C(collections).Remove(bson.M{"_id": pID, "merchant_id": mId, "selling_volume": 0})
	if err != nil {
		return errors.New("Unable to remove product")
	}
	return nil
}

type SellReport struct {
	ID         bson.ObjectId `bson:"_id", json:"_id"`
	MerchantID bson.ObjectId `bson:"merchant_id", json:"merchant_id"`
	Date       string        `bson:"date", json:"date"`
	Products   []SoldProduct `bson:"products", json:"products"`
	Accumulate float64       `bson:"accumulate", json:"accumulate"`
}

type SoldProduct struct {
	Name          string `bson:"name", json:"name"`
	SellingVolume int    `bson:"selling_volume", json:"selling_volume"`
}

func (s *Service) updateSellingVolume(pID bson.ObjectId, mID bson.ObjectId, volume int) error {
	err := s.DB.C(collections).Update(bson.M{"_id": pID, "merchant_id": mID}, bson.M{"$inc": bson.M{"selling_volume": volume}})
	if err != nil {
		return err
	}
	return nil
}

func (s *Service) findDailySellReport(mID bson.ObjectId, date string) (*SellReport, error) {
	sellReport := &SellReport{}
	err := s.DB.C(reportcollections).Find(bson.M{"merchant_id": mID, "date": date}).One(&sellReport)
	if err != nil {
		return nil, err
	}
	return sellReport, nil
}

func (s *Service) BuyProduct(sellReport *SellReport, pID bson.ObjectId, productName string, volume int) error {
	err := s.updateSellingVolume(pID, sellReport.MerchantID, volume)
	if err != nil {
		return err
	}
	dailyReport, err := s.findDailySellReport(sellReport.MerchantID, sellReport.Date)
	if err != nil {
		if err.Error() == "not found" {
			sellReport.ID = bson.NewObjectId()
			err = s.DB.C(reportcollections).Insert(&sellReport)
		}
		return err
	}

	dailyReport.Accumulate += sellReport.Accumulate

	productExists := false
	for index, product := range dailyReport.Products {
		if productName == product.Name {
			dailyReport.Products[index].SellingVolume = product.SellingVolume + volume
			productExists = true
		}
	}
	if !productExists {
		dailyReport.Products = append(dailyReport.Products, sellReport.Products[0])
	}

	err = s.DB.C(reportcollections).Update(bson.M{"_id": dailyReport.ID}, bson.M{"$set": bson.M{"products": dailyReport.Products, "accumulate": dailyReport.Accumulate}})
	if err != nil {
		return err
	}

	return nil
}
