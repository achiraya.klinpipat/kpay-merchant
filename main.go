package main

import (
	"kpay-merchant/database"
	"kpay-merchant/merchant"
	"kpay-merchant/products"

	"github.com/gin-gonic/gin"
)

func main() {
	r := gin.Default()
	db := database.DB
	connectedDB := db.Connect()

	m := &merchant.MerchantHandler{
		MerchantService: &merchant.Service{
			DB: connectedDB,
		},
	}

	p := &products.ProductsHandler{
		ProductsService: &products.Service{
			DB: connectedDB,
		},
	}

	v1 := r.Group("api/v1")

	v1.POST("/register", m.RegisterMerchant)
	v1.POST("/buy/products", p.BuyProduct)

	merchantGroup := v1.Group("/merchant/:id")
	{
		merchantGroup.Use(m.MerchantAuthentication)
		merchantGroup.GET("/", m.GetMerchantInfo)
		merchantGroup.POST("/", m.UpdateMerchant)
		merchantGroup.POST("/report", m.GetSellReports)

		merchantGroup.POST("/products", p.AllProducts)
		merchantGroup.POST("/product", p.AddProduct)
		merchantGroup.POST("/product/:product_id", p.UpdateProduct)
		merchantGroup.DELETE("/product/:product_id", p.DeleteProduct)
	}

	r.Run()
}
